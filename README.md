# jwt-token-generator

This module contains services and ui for generating new jwt tokens to use with service account access to generated service applications. The module also supports generating secrets.

The jwt tokens and secrets use the HS512 algorithm.

## Supported tags

* [`1.0.1-5591815`, `latest`, (*5591815/Dockerfile*)](https://bitbucket.org/zuunr/jwt-token-generator/src/5591815/Dockerfile)
* [`1.0.0-d187b53`, (*d187b53/Dockerfile*)](https://bitbucket.org/zuunr/jwt-token-generator/src/d187b53/Dockerfile)

## Usage

The internal entrypoint for the image is located at port 80, which will need to be exposed. The image requires you to configure a jwt secret to use when generating new tokens. Provide it as an environment variable to docker (or a Kubernetes secret).

* -e ZUUNR_CONFIG_SECURITY_JWT_SECRET='mysecret'

Start the image and then browse to http://localhost:8080 to start generating new secrets and tokens. Replace <tag> with the appropriate tag and replace <mysecret> with your jwt secret.

```shell
docker run -it --rm -p 8080:80 -e ZUUNR_CONFIG_SECURITY_JWT_SECRET='<mysecret>' zuunr/jwt-token-generator:<tag>
```

This will download the image from docker hub and start the container.
