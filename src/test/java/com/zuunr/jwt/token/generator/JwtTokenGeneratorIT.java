/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jwt.token.generator;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.zuunr.jwt.token.generator.api.JwtRequest;
import com.zuunr.jwt.token.generator.api.Role;

/**
 * <p>Integration test for creating jwt tokens and secrets.</p> 
 * 
 * <p>The Spring boot app is started at a random port.</p>
 * 
 * @author Mikael Ahlberg
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JwtTokenGeneratorIT {

    private @Autowired WebTestClient client;

    @Test
    public void givenRequestShouldCreateSecret() {
        client.post()
            .uri("/v1/jwt-token-generator/secret")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .syncBody("{}")
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().jsonPath("secret").exists();
    }

    @Test
    public void givenRequestShouldCreateToken() {
        JwtRequest request = JwtRequest.builder()
                .subject("laura_andersson")
                .ctx("sweden.stockholm")
                .timeToLiveInSeconds(3600)
                .roles(Arrays.asList(Role.builder()
                        .id("ADMIN")
                        .build()))
                .build();

        client.post()
            .uri("/v1/jwt-token-generator/token")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .syncBody(request)
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().jsonPath("token").exists();
    }
}
