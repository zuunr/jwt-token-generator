/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jwt.token.generator.api;

import javax.validation.constraints.NotEmpty;

/**
 * <p>The Role contains the role data
 * attached to the user, e.g. id = ADMIN.</p>
 * 
 * @author Mikael Ahlberg
 */
public class Role {

    @NotEmpty
    private String id;

    private Role(Builder builder) {
        this.id = builder.id;
    }

    private Role() {
        // jackson
    }

    public String getId() {
        return id;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String id;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Role build() {
            return new Role(this);
        }
    }
}
