/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jwt.token.generator;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.zuunr.jwt.token.generator.api.JwtRequest;
import com.zuunr.jwt.token.generator.jwt.JwtService;

import reactor.core.publisher.Mono;

/**
 * <p>The RestEndpoint is responsible for exposing the jwt token generator
 * services through different http endpoints.</p>
 *  
 * @author Mikael Ahlberg
 */
@CrossOrigin(origins = {"http://localhost:80", "http://localhost:8080", "http://localhost:8081"})
@RestController
public class RestEndpoint {

    private final Logger logger = LoggerFactory.getLogger(RestEndpoint.class);

    private JwtService jwtService;

    @Autowired
    public RestEndpoint(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @PostMapping(value = "/v1/jwt-token-generator/secret", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Mono<String> secret(@RequestBody String request) {
        return Mono.just(request)
                .doOnNext(o -> logger.info("/v1/jwt-token-generator/secret request: {}", o))
                .flatMap(o -> jwtService.createSecret())
                .doOnNext(o -> logger.info("/v1/jwt-token-generator/secret response: {}", o))
                .onErrorMap(this::errorMapper);
    }

    @PostMapping(value = "/v1/jwt-token-generator/token", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Mono<String> token(@Validated @RequestBody JwtRequest jwtRequest) {
        return Mono.just(jwtRequest)
                .doOnNext(o -> logger.info("/v1/jwt-token-generator/token request: {}", o))
                .flatMap(jwtService::createToken)
                .doOnNext(o -> logger.info("/v1/jwt-token-generator/token response: {}", o))
                .onErrorMap(this::errorMapper);
    }

    private Throwable errorMapper(Throwable originalCause) {
        String id = UUID.randomUUID().toString();
        String message = "An unexpected error occured, id: " + id;

        logger.error(message, originalCause);

        return new RuntimeException(message, originalCause);
    }
}
