/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jwt.token.generator.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

import java.util.Collections;

/**
 * <p>The JwtRequest contains request data required
 * for generating new jwt tokens for a user.</p>
 * 
 * @author Mikael Ahlberg
 */
public class JwtRequest {

    @NotEmpty
    private String subject;
    @Valid
    @NotEmpty
    private List<Role> roles;
    private String ctx;
    @NotNull
    private Integer timeToLiveInSeconds;

    private JwtRequest(Builder builder) {
        this.subject = builder.subject;
        this.roles = builder.roles;
        this.ctx = builder.ctx;
        this.timeToLiveInSeconds = builder.timeToLiveInSeconds;
    }

    private JwtRequest() {
        // jackson
    }

    /**
     * <p>Returns the subject, e.g. laura_andersson.</p>
     * 
     * @return a string containing the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * <p>Returns the roles, e.g. [{id = "ADMIN"}, {id = "USER"}].</p>
     * 
     * @return a string containing the roles
     */
    public List<Role> getRoles() {
        return roles;
    }

    /**
     * <p>Returns the ctx, e.g. sweden.stockholm.</p>
     * 
     * @return a string containing the ctx
     */
    public String getCtx() {
        return ctx;
    }

    /**
     * <p>Returns the time to live in seconds, e.g. 3600.</p>
     * 
     * @return an integer containing the ttl
     */
    public Integer getTimeToLiveInSeconds() {
        return timeToLiveInSeconds;
    }

    @Override
    public String toString() {
        List<JsonValue> rolesAsJson = roles.stream()
                .map(Role::getId)
                .map(JsonValue::of)
                .collect(Collectors.toList());

        return JsonObject.EMPTY
                .put("subject", subject)
                .put("roles", JsonArray.ofList(rolesAsJson))
                .put("ctx", ctx)
                .put("timeToLiveInSeconds", timeToLiveInSeconds)
                .asJson();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String subject;
        private List<Role> roles = Collections.emptyList();
        private String ctx;
        private Integer timeToLiveInSeconds;

        private Builder() {
        }

        public Builder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder roles(List<Role> roles) {
            this.roles = roles;
            return this;
        }

        public Builder ctx(String ctx) {
            this.ctx = ctx;
            return this;
        }

        public Builder timeToLiveInSeconds(Integer timeToLiveInSeconds) {
            this.timeToLiveInSeconds = timeToLiveInSeconds;
            return this;
        }

        public JwtRequest build() {
            return new JwtRequest(this);
        }
    }
}
