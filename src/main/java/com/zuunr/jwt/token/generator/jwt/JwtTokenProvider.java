/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jwt.token.generator.jwt;

import java.security.Key;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

/**
 * <p>The JwtTokenProvider is responsible for creating
 * new jwt tokens based on provided data.
 * 
 * <p>The token is signed with the {@link SignatureAlgorithm#HS512}
 * algorithm.</p>
 * 
 * <p>FIXME replace this with com.zuunr.restbed.restbed-core dependency.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class JwtTokenProvider {

    private JwtAuthenticationConfig jwtAuthenticationConfig;

    @Autowired
    public JwtTokenProvider(
            JwtAuthenticationConfig jwtAuthenticationConfig) {
        this.jwtAuthenticationConfig = jwtAuthenticationConfig;
    }

    /**
     * <p>Creates a new token based on the provided data.</p>
     * 
     * <p>
     * Example:
     *  {
     *      "ctx": "sweden.stockholm",
     *      "roles": [ USER, ADMIN ],
     *      "subject": "laura_andersson",
     *      "ttl": 3600
     *  }
     * </p>
     * 
     * @param subject is the name of the user
     * @param roles is the user's assigned roles
     * @param ctx is the user's assigned context (dot-separated hierarchy)
     * @param timeToLiveInSeconds is the time the token will live before it is expired
     * @return a signed jwt token
     */
    public String createToken(String subject, JsonArray roles, String ctx, Integer timeToLiveInSeconds) {
        Key secret = Keys.hmacShaKeyFor(jwtAuthenticationConfig.getJwtSecret().getBytes());

        Date expiration = Date.from(OffsetDateTime.now(ZoneOffset.UTC)
                .plusSeconds(timeToLiveInSeconds)
                .toInstant());

        return Jwts.builder()
                .setSubject(subject)
                .setExpiration(expiration)
                .claim("roles", roles.asMapsAndLists())
                .claim("ctx", ctx)
                .signWith(secret, SignatureAlgorithm.HS512)
                .compact();
    }
}
