/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jwt.token.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.jsonwebtoken.SignatureAlgorithm;

/**
 * <p>The application contains services for generating
 * new jwt tokens based on provided jwt secret and the
 * {@link SignatureAlgorithm#HS512} algorithm.</p>
 * 
 * <p>The application also supports creating jwt secrets.</p>
 * 
 * @author Mikael Ahlberg
 */
@SpringBootApplication(scanBasePackages = { "com.zuunr.jwt.token.generator" })
public class ApplicationMain {

    public static void main(String... args) {
        SpringApplication.run(ApplicationMain.class, args);
    }
}
