/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jwt.token.generator.jwt;

import java.security.Key;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.jwt.token.generator.api.JwtRequest;
import com.zuunr.jwt.token.generator.api.Role;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import reactor.core.publisher.Mono;

/**
 * <p>The JwtService contains the services to create
 * new tokens and secrets using the {@link SignatureAlgorithm#HS512}
 * algorithm.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class JwtService {

    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    public JwtService(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    /**
     * <p> Creates a new secret using the {@link SignatureAlgorithm#HS512}
     * algorithm and returns a json representation of that secret.</p>
     * 
     * @return a mono string containing the new secret as json
     */
    public Mono<String> createSecret() {
        Key secret = Keys.secretKeyFor(SignatureAlgorithm.HS512);

        return Mono.just(JsonObject.EMPTY
                .put("secret", Base64.getEncoder().encodeToString(secret.getEncoded()))
                .asJson());
    }

    /**
     * <p>Creates a new token using the provided {@link JwtRequest}.</p>
     * 
     * <p>{@link JwtTokenProvider} should be replaced with zuunr.runtime.core.</p>
     * 
     * @param jwtRequest contains the request data to use when creating the token
     * @return a mono string containing the signed token as json
     */
    public Mono<String> createToken(JwtRequest jwtRequest) {
        JsonArray roles = JsonArray.ofList(convertRoles(jwtRequest.getRoles()));

        return Mono.just(jwtTokenProvider.createToken(jwtRequest.getSubject(), roles, jwtRequest.getCtx(), jwtRequest.getTimeToLiveInSeconds()))
                .map(o -> JsonObject.EMPTY.put("token", o).asJson());
    }

    private List<JsonValue> convertRoles(List<Role> roles) {
        return roles.stream()
                .map(Role::getId)
                .map(JsonValue::of)
                .collect(Collectors.toList());
    }
}
